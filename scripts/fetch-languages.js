const fs = require("fs");
const yaml = require("yaml");

if (!process.env.GITHUB_TOKEN || !process.env.GITLAB_TOKEN) {
	console.error("missing tokens");
	process.exit(-1);
}

const languages = {};
const initTasks = [];
const tasks = [];


function incr(lang) {
	if (!languages[lang]) {
		languages[lang] = 1;
	} else {
		languages[lang]++;
	}
}

// <https://api.github.com/user/repos?per_page=100&visibility=all&page=2>; rel="next", <https://api.github.com/user/repos?per_page=100&visibility=all&page=2>; rel="last"
const nextUrl = /^<(.+)>; rel="next"/;

function iteratePages(res, callback) {
	const parse = nextUrl.exec(res.headers["link"]);
	if (!parse) return;

	const url = parse[1];
	if (!url) return;

	callback(url);
}

function wait() {
	return new Promise((res, rej) => setTimeout(res, 500));
}

async function jsonOrErr(r) {
	if (r.status !== 200) {
		throw new Error("Invalide response code " + JSON.stringify(await r.json()))
	}
	const json = await r.json();
	return json;
}

async function iterGithub(r) {
	if (!Array.isArray(r)) {
		console.log("not array", r);
	}
	for (const repo of r) {
		if (repo.fork && !repo.private) {
			continue;
		}

		await wait();

		tasks.push(fetch(repo.languages_url, {
			headers: { Authorization: "Bearer " + process.env.GITHUB_TOKEN }
		})
			.then(jsonOrErr)
			.then(r => {
				console.log("github", repo.full_name, Object.keys(r));
				for (const lang in r) {
					incr(lang);
				}
			}));
	}
}

initTasks.push(fetch("https://api.github.com/user/repos?per_page=100&visibility=all", {
	headers: { Authorization: "Bearer " + process.env.GITHUB_TOKEN }
})
	.then(jsonOrErr)
	.then(iterGithub));

initTasks.push(fetch("https://api.github.com/user/repos?per_page=100&visibility=all&page=2", {
	headers: { Authorization: "Bearer " + process.env.GITHUB_TOKEN }
})
	.then(jsonOrErr)
	.then(iterGithub));


initTasks.push(fetch("https://gitlab.com/api/v4/users/2363518/projects", {
	headers: { "PRIVATE-TOKEN": process.env.GITLAB_TOKEN }
})
	.then(jsonOrErr)
	.then(r => {
		if (!Array.isArray(r)) {
			console.log("not array", r);
		}
		for (const repo of r) {
			if (repo.forked_from_project) {
				continue;
			}

			tasks.push(fetch(`https://gitlab.com/api/v4/projects/${repo.id}/languages`, {
				headers: { "PRIVATE-TOKEN": process.env.GITLAB_TOKEN }
			})
				.then(jsonOrErr)
				.then(r => {
					console.log("gitlab", repo.path_with_namespace, Object.keys(r));
					for (const lang in r) {
						incr(lang);
					}
				}));
		}
	}));

Promise.all(initTasks).then(a => {
	Promise.all(tasks).then(async b => {
		const linguist = yaml.parse(await (await fetch("https://raw.githubusercontent.com/github/linguist/master/lib/linguist/languages.yml")).text());

		const nivoLangs = [];
		for (const l in languages) {
			if (!linguist[l]) console.warn(`language ${l} not found in linguist`);
			nivoLangs.push({ id: l, value: languages[l], color: linguist[l].color });
		}
		nivoLangs.sort((a, b) => a.value - b.value);
		fs.writeFileSync("./src/languages.json", JSON.stringify(nivoLangs));
	});
});
