import { Card } from "./components/Card";
import { List, ListItem } from "./components/List";
import Button from "./components/Button";
import { contacts } from "./lists";

function Contact() {
	return (
		<Card>
			<List>
				{contacts.map((e, i) => {
					return (
						<a
							href={e.url}
							target="_blank"
							rel="noreferrer"
							style={{ textDecoration: "none" }}
						>
							<ListItem key={i} action>
								<div
									className="feature-icon"
									dangerouslySetInnerHTML={{
										__html: e.svg,
									}}
									style={{ fill: e.color }}
								></div>
								<Button noRipple>{e.text}</Button>
							</ListItem>
						</a>
					);
				})}
			</List>
		</Card>
	);
}

export default Contact;
