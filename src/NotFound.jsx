import { List, ListItem } from "./components/List";

function NotFound() {
	return (
		<div>
			<h2>OwO what's this?</h2>
			<p>It looks like you're lost, here have a snack</p>
			<List>
				<ListItem
					action
					onClick={() => {
						console.log("The best food");
						window.open("https://emojipedia.org/cookie/").focus();
					}}
				>
					<h1>🍪</h1>
				</ListItem>
				<ListItem
					action
					onClick={() => {
						console.log("The best beverage");
						let audio = new Audio("/assets/coffee.mp3"); // https://freesound.org/people/Acekat13/sounds/515685/
						audio.play();
						setTimeout(() => {
							window.open("http://coffee.vinceh121.me").focus();
						}, 22000);
					}}
				>
					<h1>☕</h1>
				</ListItem>
			</List>
		</div>
	);
}

export default NotFound;
