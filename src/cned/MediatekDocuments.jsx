import { Card, CardHeader, CardHeaderText, CardSubtitle, CardTitle } from "../components/Card";
import Divider from "../components/Divider";
import FeatureList from "../components/FeatureList";
import { DrawMediatekDocuments } from "./Drawio";

const lst = [
	{
		head: "GH",
		text: "Code source",
		url: "https://github.com/vinceh121/mediatekdocuments"
	}, {
		head: "DT",
		text: "Documentation technique frontend",
		url: "https://mediatekdocuments.vinceh121.me/docsfront/"
	}, {
		head: "DT",
		text: "Documentation technique backend",
		url: "https://mediatekdocuments.vinceh121.me/docs/"
	}, {
		head: "CC",
		text: "Cahier des Charges",
		url: "https://cdn.vinceh121.me/cahier_des_charges_AP3.pdf"
	}, {
		head: "MM",
		text: "Missions",
		url: "https://cdn.vinceh121.me/AP3_missions.pdf"
	}, {
		head: "CD",
		text: "Contrat",
		url: "https://mediatekformation.vinceh121.me/docs/contratDeveloppement.pdf" // old one is on purpose
	}, {
		head: "CR",
		text: "Compte Rendu",
		url: "https://cdn.vinceh121.me/CompteRendu_AP3.pdf"
	}, {
		head: "PV",
		text: "PV Recette",
		url: "https://cdn.vinceh121.me/pv_recette_AP3.pdf"
	}, {
		head: "NS",
		text: "Application",
		url: "https://cdn.vinceh121.me/installer-mediatek-documents.exe"
	}
];

function MediaTekDocuments() {
	return (
		<>
			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Mediatek Documents</CardTitle>
						<CardSubtitle>Gestion de médiathèque</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>
					Mediatek Documents est une application de gestion documentaire de médiathèque.
					Elle permet de gérer les livres, DVD, revues, ainsi que les prêts.
				</p>
				<p>L'application est développée sous forme d'application de bureau C# étant un client d'un serveur d'API REST en PHP.</p>

				<p>Pile technologique :</p>
				<ul>
					<li>.NET 8.0/C#</li>
					<li>GTK#</li>
					<li>PHP 8.0</li>
					<li>CodeIgniter 4</li>
				</ul>

				<p>Compétences couvertes :</p>
				<ul>
					<li>B1.1 : Gérer le patrimoine informatique</li>
					<li>B1.2 : Répondre aux incidents et aux demandes d'assistance et d'évolution</li>
					<li>B1.4 : Travailler en mode projet</li>
					<li>B1.5 : Mettre à disposition des utilisateurs un service informatique</li>
					<li>B2.1 : Concevoir et développer une solution applicative</li>
					<li>B2.2 : Assurer la maintenance corrective ou évolutive d'une solution</li>
					<li>B2.3 : Gérer les données</li>
				</ul>

			</Card>

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Architecture</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<DrawMediatekDocuments />
			</Card>


			<Divider />

			<FeatureList features={lst} title="Liens" />

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Demo application</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<iframe
					style={{ width: "100%", height: "40vw" }}
					title="Mediatek Documents Demo"
					src="https://tube.vinceh121.me/videos/embed/229d484a-c9e4-4609-b534-405ee20c8043"
					allowfullscreen=""
					sandbox="allow-same-origin allow-scripts allow-popups"
					frameborder="0"></iframe>
			</Card>
		</>
	);
}

export default MediaTekDocuments;
