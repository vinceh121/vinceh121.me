import { Card, CardHeader, CardHeaderText, CardSubtitle, CardTitle } from "../components/Card";
import Divider from "../components/Divider";
import FeatureList from "../components/FeatureList";
import { DrawMediatek } from "./Drawio";

const lst = [
	{
		head: "GH",
		text: "Code source",
		url: "https://github.com/vinceh121/CNED-Mediatek"
	}, {
		head: "DT",
		text: "Documentation technique",
		url: "https://mediatek.vinceh121.me/doc/html"
	}, {
		head: "HP",
		text: "Page principale",
		url: "https://mediatek.vinceh121.me"
	}
];

function MediaTek() {
	return (
		<>
			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>MediaTek86</CardTitle>
						<CardSubtitle>Un exercice d'application métier</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>MediaTek86 est une entreprise gérant des médiathèques dans la Vienne.</p>
				<p>La cible de la mission est une application riche monoposte à être installée sur le poste administratif de chaque médiathèque.
					Cette application de bureau doit pouvoir gérer la liste d'employés de la médiathèque.</p>

			</Card>

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Architecture</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<DrawMediatek />
			</Card>

			<Divider />

			<FeatureList features={lst} title="Liens" />

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Demo</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<iframe
					style={{ width: "100%", height: "40vw" }}
					title="Demo MediaTek86"
					src="https://tube.vinceh121.me/videos/embed/68c01d19-04fd-4f00-bd2f-90279f4ce0ab"
					allowfullscreen=""
					sandbox="allow-same-origin allow-scripts allow-popups"
					frameborder="0"></iframe>
			</Card>
		</>
	);
}

export default MediaTek;
