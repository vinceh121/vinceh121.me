import "./PortfolioE4.css";
import { Card, CardHeader, CardHeaderText, CardSubtitle, CardTitle } from "../components/Card";
import Divider from "../components/Divider";
import Link from "../components/Link";
import IconLink from "../components/IconLink";
import chartTheme from "../chartTheme.json";
import { Pie } from "@nivo/pie";
import SimpleIcon from "../components/SimpleIcon";
import {
	siCsharp,
	siGtk,
	siPhp,
	siSymfony,
	siJquery,
	siWebpack,
	siBootstrap,
	siMariadb,
	siCodeigniter,
	siJavascript,
	siMicrosoftexcel,
	siHtml5,
	siLibreofficecalc,
	siEclipsevertdotx,
	siWeblate,
	siAdobeacrobatreader
} from "simple-icons";
import HorizontalSeparator from "../components/HorizontalSeparator";
import { ResponsiveSankey } from "@nivo/sankey";
import CaptionedImage from "../components/CaptionedImage";
import IconExternalLink from "../components/IconExternalLink";
import { useEffect } from "preact/hooks";
import { createRef } from "preact";
import WordCloud from "../components/WordCloud";
import { DrawMediatek, DrawMediatekDocuments, DrawMediatekFormation } from "./Drawio";

const projectsTooltip = ({ datum }) => <ul className="chart-tooltip">{datum.data.projects.map(p => <li>{p}</li>)}</ul>;

function PortfolioE4() {
	const ref = createRef();

	useEffect(() => {
		setTimeout(() => {
			ref.current.scrollIntoView({ behavior: "smooth" });
		}, 500);
	});

	return (
		<>
			{/* <Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Présentation type E4</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<iframe
					style={{ width: "100%", height: "40vw" }}
					title="Presentation portfolio"
					src="https://tube.vinceh121.me/videos/embed/7c21bec5-8463-4b6f-9622-409ce6118973"
					allowfullscreen=""
					sandbox="allow-same-origin allow-scripts allow-popups"
					frameborder="0"></iframe>
			</Card>

			<Divider /> */}

			<span ref={ref}></span>
			<div class="jumbotron">
				<h1>Vincent Hyvert</h1>
				<h2>D&eacute;veloppeur Fullstack</h2>
				<WordCloud words={[
					'Fullstack',
					'Front-end',
					'Back-end',
					'JavaScript',
					'HTML',
					'CSS',
					'LESS',
					'Sass',
					'Java',
					'C++',
					'C',
					'Bash',
					'Python',
					'TypeScript',
					'PHP',
					'Docker',
					'SSH',
					'Git',
					'I18N',
					'HTTP',
					'CI',
					'CD',
					'GIS',
					'REST',
					'OpenAPI',
					'JSON',
					'SQL',
					'MySQL',
					'PostgreSQL',
					'KeyCloak',
					'OpenID Connect',
					'Symfony',
					'CodeIgniter',
					'C#',
					'MongoDB',
					'Redis',
					'Vert.x',
					'Debian',
					'systemd',
					'Nginx',
					'Apache',
					'RethinkDB',
					'SMTP',
					'Postfix',
					'Django',
					'Express.js',
					'Spring',
					'Linux',
					'VPS',
					'Cloud',
					'JSON:API',
					'InfluxDB',
					'WebSocket',
					'WHOIS',
					'gettext',
					'OAuth',
					'Node.js',
					'Native bridge',
					'Apereo CAS',
					'OpenStreetMap',
					'Overpass-API',
					'Kanban',
					'Agile',
					'Android',
					'Flutter',
					'Dart',
					'Twig',
					'Webpack',
					'Vite',
					'React',
					'Angular',
					'Vue'
				]} />
			</div>

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Développement professionnel personnel</CardTitle>
					</CardHeaderText>
				</CardHeader>

				<h3>Veille technologique</h3>
				<ul>
					<li>Suivi de la mailing list <a href="https://lists.debian.org/debian-security-announce/" target="_blank">debian-security-announce</a></li>
					<li>Suivi du <a href="https://www.cert.ssi.gouv.fr/" target="_blank">CERT-FR</a> via RSS</li>
					<li>Suivi de la mailing list <a href="https://mail.openjdk.org/mailman/listinfo/announce">OpenJDK announce</a></li>
				</ul>

				<h3>Contributions opensource</h3>
				<ul>
					<li><img src="https://avatars.githubusercontent.com/u/13812895?s=48&v=4" alt="Logo JNA" width={24} height={24} />Java Native Access : bibliothèque facilitant le développement de ponts natif</li>
					<ul>
						<li><a href="https://github.com/java-native-access/jna/pull/1602">Implémentation de fonctions X11</a></li>
					</ul>
					<li><SimpleIcon icon={siEclipsevertdotx} /> Vert.x : framework web reactive</li>
					<ul>
						<li><a href="https://github.com/vert-x3/vertx-web/pull/2592">Bugfix dans le cache du client HTTP</a></li>
					</ul>
					<li><SimpleIcon icon={siWeblate} />  Weblate : plateforme de traduction en continu</li>
					<ul>
						<li><a href="https://github.com/WeblateOrg/weblate/pull/10453">Routes d&apos;API REST permettant de labelliser les unités</a></li>
						<li><a href="https://github.com/WeblateOrg/weblate/pull/11128">Bugfix a l&apos;utilisation d&apos;utilisateurs robot</a></li>
					</ul>
					<li><img src="https://avatars.githubusercontent.com/u/34995903?s=48&v=4" width={24} height={24} /> LibreCaptcha : service de génération de captchas</li>
					<ul>
						<li><a href="https://github.com/librecaptcha/lc-core/pull/94">Configuration du port d'écoute du serveur HTTP</a></li>
					</ul>
				</ul>

				<h3>Contributions de traduction</h3>
				<ul>
					<li><a href="https://translations.launchpad.net/~vinceh121">Canonical Launchpad</a></li>
					<li><a href="https://hosted.weblate.org/user/vinceh121/#changes">Weblated Hosted</a></li>
					<li><a href="https://weblate.vinceh121.me/user/vinceh121/#contributed">weblate.vinceh121.me</a></li>
				</ul>

				<h3>Projets personnels</h3>
				<ul>
					<li><a href="https://gmc.vinceh121.me" target="_blank">GMCserver</a> : service web pour enregistrer, analyser et relayer les enregistrements de compteur Geigers</li>
					<li><a href="https://jpronote.vinceh121.me" target="_blank">jpronote</a> : wrapper d&apos;API Java pour l&apos;ENT Pronote</li>
					<li><a href="https://github.com/vinceh121/jskolengo" target="_blank">jskolengo</a> : wrapper d&apos;API Java pour l&apos;ENT Skolengo</li>
				</ul>
			</Card>

			<Divider large />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Stages</CardTitle>
					</CardHeaderText>
				</CardHeader>

				<img src="https://i.vinceh121.me/ySP2eXpn.png" alt="Logo Iwit Systems" />

				<ul>
					<li>Développement PHP, Symfony, JQuery</li>
					<li>Migrations de bases de données, d&apos;anciennes données incorrectes</li>
					<li>Templates de mail</li>
					<li>Authentification par magic links</li>
					<li>Interface client de ticketing</li>
					<li>Gestion de park informatique</li>
					<li>Déploiements en test</li>
					<li>Configuration de serveurs via SSH</li>
					<li>Mise en place d'optimisation sur le build des assets en pipeline</li>
					<li>Pair programming</li>
					<li>Réunions de gestion de projet</li>
					<li>Revue de code</li>
					<li>Déploiement d&apos;images Docker</li>
					<li>Méthode Agile</li>
				</ul>
			</Card>

			<Divider large />

			<Card>
				<div>
					<a href="https://cdn.vinceh121.me/upload/E4_tableau_de_synthese_prerempli.ods" target="_blank"><SimpleIcon icon={siLibreofficecalc} /></a>
					<a href="https://cdn.vinceh121.me/upload/E4_tableau_de_synthese_prerempli.xlsx" target="_blank"><SimpleIcon icon={siMicrosoftexcel} /></a>
					<a href="https://cdn.vinceh121.me/upload/E4_tableau_de_synthese_prerempli.pdf" target="_blank"><SimpleIcon icon={siAdobeacrobatreader} /></a>
				</div>

				<iframe src="https://cdn.vinceh121.me/upload/E4_tableau_de_synthese_prerempli.pdf" loading="lazy" width="100%" height="400px"></iframe>
			</Card>

			<Divider large />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Langages &amp; technologies</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<div className="charts-container">
					<div className="chart">
						<Pie
							data={[
								{
									id: 'C#',
									value: 2,
									color: "#" + siCsharp.hex,
									projects: ['Mediatek86', 'Mediatek Documents'],
								},
								{
									id: 'PHP',
									value: 3,
									color: "#" + siPhp.hex,
									projects: ['Mediatek Formation', 'Mediatek Documents'],
								},
								{
									id: 'JavaScript',
									value: 1,
									color: "#" + siJavascript.hex,
									projects: ['Mediatek Formation'],
								},
							]}
							tooltip={projectsTooltip}
							colors={{ datum: "data.color" }}
							margin={{ top: 20, right: 100, bottom: 20, left: 100 }}
							sortByValue={true}
							theme={chartTheme}
							arcLinkLabelsDiagonalLength={24}
							arcLinkLabelsStraightLength={16}
							activeOuterRadiusOffset={24}
							transitionMode="middleAngle"
							motionConfig="stiff"
							enableArcLabels={false}
							width={500}
							height={500} />
					</div>
					<div className="chart">
						<Pie
							data={[
								{
									id: 'MariaDB',
									value: 3,
									color: "#" + siMariadb.hex,
									projects: ['Mediatek86', 'Mediatek Formation', 'Mediatek Documents']
								},
								{
									id: 'Symfony',
									value: 1,
									color: "#" + siSymfony.hex,
									projects: ['Mediatek Formation']
								},
								{
									id: 'GTK#',
									value: 2,
									color: "#" + siGtk.hex,
									projects: ['Mediatek86', 'Mediatek Documents']
								},
								{
									id: 'CodeIgniter',
									value: 1,
									color: "#" + siCodeigniter.hex,
									projects: ['Mediatek Documents']
								},
							]}
							tooltip={projectsTooltip}
							colors={{ datum: "data.color" }}
							margin={{ top: 20, right: 100, bottom: 20, left: 100 }}
							sortByValue={true}
							theme={chartTheme}
							arcLinkLabelsDiagonalLength={24}
							arcLinkLabelsStraightLength={16}
							activeOuterRadiusOffset={24}
							transitionMode="middleAngle"
							motionConfig="stiff"
							enableArcLabels={false}
							width={500}
							height={500} />
					</div>
				</div>
				<div className="card-center" style={{ height: "500px" }}>
					<ResponsiveSankey
						data={{
							nodes: [
								{
									id: "PHP",
									nodeColor: "#4F5D95"
								},
								{
									id: "JavaScript",
									nodeColor: "#f1e05a"
								},
								{
									id: "C#",
									nodeColor: "#178600"
								},

								{
									id: "MariaDB",
									nodeColor: "#" + siMariadb.hex
								},
								{
									id: "CodeIgniter",
									nodeColor: "#" + siCodeigniter.hex
								},
								{
									id: "GTK#",
									nodeColor: "#" + siGtk.hex
								},
								{
									id: "Symfony",
									nodeColor: "#" + siSymfony.hex
								},

								{
									id: "MediaTek86",
									nodeColor: "#05abc1"
								},
								{
									id: "Mediatek Formation",
									nodeColor: "#0dcaf0"
								},
								{
									id: "Mediatek Documents",
									nodeColor: "#68a9a2"
								}
							],
							links: [
								{
									source: "PHP",
									target: "CodeIgniter",
									value: 1
								},
								{
									source: "PHP",
									target: "MariaDB",
									value: 2
								},
								{
									source: "PHP",
									target: "Symfony",
									value: 1
								},
								{
									source: "JavaScript",
									target: "Symfony",
									value: 1
								},
								{
									source: "C#",
									target: "GTK#",
									value: 2
								},
								{
									source: "C#",
									target: "MariaDB",
									value: 1
								},

								{
									source: "MariaDB",
									target: "MediaTek86",
									value: 1
								},
								{
									source: "MariaDB",
									target: "Mediatek Formation",
									value: 1
								},
								{
									source: "MariaDB",
									target: "Mediatek Documents",
									value: 1
								},
								{
									source: "CodeIgniter",
									target: "Mediatek Documents",
									value: 1
								},
								{
									source: "Symfony",
									target: "Mediatek Formation",
									value: 1
								},
								{
									source: "GTK#",
									target: "MediaTek86",
									value: 1
								},
								{
									source: "GTK#",
									target: "Mediatek Documents",
									value: 1
								}
							]
						}}
						nodeTooltip={() => { }}
						linkTooltip={() => { }}
						transitionMode="middleAngle"
						motionConfig="stiff"
						enableArcLabels={false}
						colors={{ datum: "nodeColor" }}
						labelTextColor="black"
						margin={{ top: 50, right: 20, bottom: 20, left: 50 }}
						height={500} />
				</div>
			</Card>

			<Divider large />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>
							<Link to="/cned/mediatek">Mediatek 86 <IconLink /></Link>
							<HorizontalSeparator space={2} /><SimpleIcon icon={siCsharp} brighten={true} />
							<SimpleIcon icon={siGtk} /><SimpleIcon icon={siMariadb} />
						</CardTitle>
						<CardSubtitle>
							MediaTek86 est une application riche pour les responsables de médiathèques.
						</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>
					Elle permet de gérer les employés d&apos;une médiathèque ainsi que de leurs absences.
				</p>
				<p>
					Elle est développée en C#, en utilisant la bibliothèque graphique GtkSharp ainsi que la base de
					données MariaDB.
				</p>

				<ul>
					<li>B1.2 : Répondre aux incidents et aux demandes d'assistance et d'évolution</li>
					<li>B1.4 : Travailler en mode projet</li>
					<li>B1.5 : Mettre à disposition des utilisateurs un service informatique</li>
					<li>B2.1 : Concevoir et développer une solution applicative</li>
					<li>B2.2 : Assurer la maintenance corrective ou évolutive d'une solution</li>
					<li>B2.3 : Gérer les données</li>
				</ul>

				<DrawMediatek />

				<div class="card-center">
					<CaptionedImage alt="Capture d'écran Mediatek86" description="Tableau des employés" src="https://i.vinceh121.me/HfkSr21n.png" />
				</div>
				<div class="card-center">
					<CaptionedImage alt="Capture d'écran calendrier Mediatek86" description="Calendrier des absences" src="https://i.vinceh121.me/f7iWyEqy.png" />
				</div>
			</Card>

			<Divider large />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>
							<Link to="/cned/mediatekformation">Mediatek Formation<IconLink /></Link>
							<HorizontalSeparator space={2} /><SimpleIcon icon={siPhp} /><SimpleIcon icon={siSymfony} />
							<SimpleIcon icon={siWebpack} /><SimpleIcon icon={siJavascript} /><SimpleIcon icon={siJquery} />
							<SimpleIcon icon={siBootstrap} /><SimpleIcon icon={siMariadb} />
						</CardTitle>
						<CardSubtitle>
							Mediatek Formation est une application web permettant de présenter et organiser des
							formations vidéo.
						</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>La cible de la mission est une application web listant plusieurs formations sous forme de vidéos
					Youtube.
					Elles sont organisées en playlists et catégories.</p>

				<p>Elle comporte un frontoffice ainsi qu'un backoffice sécurisé par un SSO Keycloak.</p>

				<p>Backend développé en PHP avec le framework Symfony et le langage de templating Twig.</p>
				<p>Frontend développé avec les frameworks JQuery et Bootstrap.</p>

				<p>Compétences couvertes :</p>
				<ul>
					<li>B1.1 : Gérer le patrimoine informatique</li>
					<li>B1.2 : Répondre aux incidents et aux demandes d'assistance et d'évolution</li>
					<li>B1.3 : Développer la présence en ligne de l'organisation</li>
					<li>B1.4 : Travailler en mode projet</li>
					<li>B1.5 : Mettre à disposition des utilisateurs un service informatique</li>
					<li>B2.1 : Concevoir et développer une solution applicative</li>
					<li>B2.2 : Assurer la maintenance corrective ou évolutive d'une solution</li>
					<li>B2.3 : Gérer les données</li>
				</ul>

				<DrawMediatekFormation />

				<div className="card-center">
					<CaptionedImage alt="Frontoffice Mediatek Formation" description={
						<>
							<a href="https://mediatekformation.vinceh121.me/" target="_blank">Ouvrir</a><IconExternalLink />
						</>
					}
						src="https://i.vinceh121.me/JkWPXV6k.png" />
				</div>
				<div className="card-center">
					<CaptionedImage alt="Backoffice Mediatek Formation" description={
						<>
							<a href="https://mediatekformation.vinceh121.me/admin" target="_blank">Ouvrir</a><IconExternalLink />
						</>
					}
						src="https://i.vinceh121.me/3oIk4jDS.png" />
				</div>
			</Card>

			<Divider large />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>
							<Link to="/cned/mediatekdocuments">Mediatek Documents<IconLink /></Link>
							<HorizontalSeparator space={2} /><SimpleIcon icon={siCsharp} brighten={true} /><SimpleIcon icon={siGtk} />
							<SimpleIcon icon={siPhp} /><SimpleIcon icon={siCodeigniter} /><SimpleIcon icon={siMariadb} />
						</CardTitle>
						<CardSubtitle>
							Mediatek Documents permet de gérer les documents (livres, DVD, revues) d'une médiathèque.
						</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>Développé avec un client en C# et GTK#, et une API REST développée en PHP et CodeIgniter.</p>

				<p>Compétences couvertes :</p>
				<ul>
					<li>B1.1 : Gérer le patrimoine informatique</li>
					<li>B1.2 : Répondre aux incidents et aux demandes d'assistance et d'évolution</li>
					<li>B1.4 : Travailler en mode projet</li>
					<li>B1.5 : Mettre à disposition des utilisateurs un service informatique</li>
					<li>B2.1 : Concevoir et développer une solution applicative</li>
					<li>B2.2 : Assurer la maintenance corrective ou évolutive d'une solution</li>
					<li>B2.3 : Gérer les données</li>
				</ul>

				<DrawMediatekDocuments />

				<div className="card-center">
					<CaptionedImage alt="Application Mediatek Documents" src="https://i.vinceh121.me/WyEIbopy.png" />
				</div>
				<div className="card-center">
					<CaptionedImage alt="API REST Mediatek Documents" description="Collection OpenAPI réalisée avec Insomnia"
						src="https://i.vinceh121.me/eiCkFluP.png" />
				</div>
			</Card>
		</>
	);
}

export default PortfolioE4;
