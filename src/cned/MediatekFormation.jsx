import { Card, CardHeader, CardHeaderText, CardSubtitle, CardTitle } from "../components/Card";
import Divider from "../components/Divider";
import FeatureList from "../components/FeatureList";
import { DrawMediatekFormation } from "./Drawio";

const lst = [
	{
		head: "GH",
		text: "Code source",
		url: "https://github.com/vinceh121/mediatekformation"
	}, {
		head: "DT",
		text: "Documentation technique",
		url: "https://mediatekformation.vinceh121.me/docs/"
	}, {
		head: "CC",
		text: "Cahier des Charges",
		url: "https://mediatekformation.vinceh121.me/docs/cahier_des_charges.pdf"
	}, {
		head: "MM",
		text: "Missions",
		url: "https://mediatekformation.vinceh121.me/docs/AP1_missions.pdf"
	}, {
		head: "CD",
		text: "Contrat",
		url: "https://mediatekformation.vinceh121.me/docs/contratDeveloppement.pdf"
	}, {
		head: "CR",
		text: "Compte Rendu",
		url: "https://mediatekformation.vinceh121.me/docs/CompteRendu.pdf"
	}, {
		head: "PV",
		text: "PV Recette",
		url: "https://mediatekformation.vinceh121.me/docs/pv_recette.pdf"
	}, {
		head: "HP",
		text: "Page principale",
		url: "https://mediatekformation.vinceh121.me"
	}, {
		head: "BO",
		text: "Backoffice",
		url: "https://mediatekformation.vinceh121.me/admin"
	}
];

function MediaTekFormation() {
	return (
		<>
			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Mediatek Formation</CardTitle>
						<CardSubtitle>Formations vidéo</CardSubtitle>
					</CardHeaderText>
				</CardHeader>

				<p>MediaTek86 est une entreprise gérant des médiathèques dans la Vienne.</p>
				<p>La cible de la mission est une application web listant plusieurs formations sous forme de vidéos Youtube.
					Elles sont organisées en playlists et catégories.</p>

				<p>Pile technologique :</p>
				<ul>
					<li>Symfony 5.4</li>
					<li>Bootstrap 5</li>
					<li>JQuery</li>
				</ul>

				<p>Compétences couvertes :</p>
				<ul>
					<li>B1.1 : Gérer le patrimoine informatique</li>
					<li>B1.2 : Répondre aux incidents et aux demandes d'assistance et d'évolution</li>
					<li>B1.3 : Développer la présence en ligne de l'organisation</li>
					<li>B1.4 : Travailler en mode projet</li>
					<li>B1.5 : Mettre à disposition des utilisateurs un service informatique</li>
					<li>B2.1 : Concevoir et développer une solution applicative</li>
					<li>B2.2 : Assurer la maintenance corrective ou évolutive d'une solution</li>
					<li>B2.3 : Gérer les données</li>
				</ul>

			</Card>

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText><CardTitle>Architecture</CardTitle></CardHeaderText>
				</CardHeader>
				<DrawMediatekFormation />
			</Card>

			<Divider />

			<FeatureList features={lst} title="Liens" />

			<Divider />

			<Card>
				<CardHeader>
					<CardHeaderText>
						<CardTitle>Demo Front + Backoffice</CardTitle>
					</CardHeaderText>
				</CardHeader>
				<iframe
					style={{ width: "100%", height: "40vw" }}
					title="Demo Mediatek Formation"
					src="https://tube.vinceh121.me/videos/embed/fcc25527-9e70-4495-8964-5dda51b83810"
					allowfullscreen=""
					sandbox="allow-same-origin allow-scripts allow-popups"
					frameborder="0"></iframe>
			</Card>
		</>
	);
}

export default MediaTekFormation;
