import { memo } from "react";
import "./FeatureList.css";
import { Card, CardTitle } from "./Card";
import { List, ListItem } from "./List";
import IconExternalLink from "./IconExternalLink";

const ThingsIDo = memo((props) => {
	return (
		<Card>
			<CardTitle>{props.title}</CardTitle>
			<List>
				{props.features.map((e, i) => {
					const item = <ListItem key={i} action={Boolean(e.url)}>
						{
							e.head ? <div className="feature-header">
								{e.head}
							</div> : undefined
						}
						{
							e.svg ?
								<div
									className="feature-icon"
									dangerouslySetInnerHTML={{ __html: e.svg }}
									style={{ fill: e.color }}
								></div> :
								e.imgUrl ? <img alt="" src={e.imgUrl} className="feature-icon" /> :
									undefined
						}
						<div className="feature-item">
							<div className="feature-title">
								{e.text}
								{e.url ? <span style={{ paddingLeft: "5px" }}><IconExternalLink /></span> : undefined}
							</div>
							<div class="feature-desc">
								{e.desc}
							</div>
						</div>
					</ListItem>;

					if (e.url) {
						return <a
							href={e.url}
							target="_blank"
							rel="noreferrer"
							style={{ textDecoration: "none" }}
						>
							{item}
						</a>
					} else {
						return item;
					}

				})}
			</List>
		</Card>
	);
});

export default ThingsIDo;
