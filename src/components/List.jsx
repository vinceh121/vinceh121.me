import { memo } from "react";
import "./List.css";
import Ripples from "react-ripples";

const List = memo((props) => {
	return (
		<div className="list" {...props}>
			{props.children}
		</div>
	);
});

const ListItem = memo((props) => {
	const { action, ...rest } = props;

	const inner = (
		<div
			className={"list-item" + (action ? " list-item-action" : "")}
			{...rest}
		>
			{props.children}
		</div>
	);

	if (action) {
		return <Ripples>{inner}</Ripples>;
	}

	return inner;
});

export { List, ListItem };
