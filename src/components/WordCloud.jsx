import './WordCloud.css';

const shuffle = (input) => {
	const output = [...input];

	for (let i = input.length; i > 1; i--) {
		const j = Math.floor(Math.random() * input.length);
		const tmp = output[i - 1];
		output[i - 1] = output[j];
		output[j] = tmp;
	}

	return output;
};

const makeWord = (word) => {
	return <span style={{ animationDelay: (Math.random() * 10) + 's' }}>{word}</span>;
};

function WordCloud({ words }) {
	return <div className="word-cloud">
		{shuffle(words).map(makeWord)}
	</div>;
}

export default WordCloud;
