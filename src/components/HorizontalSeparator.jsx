import './HorizontalSeparator.css';

function HorizontalSeparator({ space }) {
	return <span className="horizontal-separator" style={{ width: space + 'em' }}></span>;
}

export default HorizontalSeparator;
