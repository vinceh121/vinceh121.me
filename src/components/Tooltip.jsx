import "./Tooltip.css";
import { useState } from 'preact/hooks';
import { memo } from "react";

const Tooltip = memo((props) => {
	const { title, ...rest } = props;

	const [visible, setVisible] = useState();

	const onEnter = () => {
		setVisible(true);
	};

	const onLeave = () => {
		setVisible(false);
	};

	return <div className="tooltip-wrapper" onMouseEnter={onEnter} onMouseLeave={onLeave} {...rest}>
		{props.children}
		<div className={"tooltip" + (visible ? " tooltip-visible" : "")}>{title}</div>
	</div>;
});

export default Tooltip;
