import {memo} from "react";
import Ripples from "react-ripples";
import {useLocation} from "wouter";
import "./Button.css";

const ButtonInner = memo((props) => {
	let classes = ["button"];

	if (props.primary) {
		classes.push("button-primary");
	}

	if (props.to) {
		return (
			<button className={classes.join(" ")} type="button">
				{props.children}
			</button>
		);
	} else {
		return (
			<a className={classes.join(" ")} href={props.href}>
				{props.children}
			</a>
		);
	}
});

const Button = memo((props) => {
	const [_, setLocation] = useLocation();

	const onClick = () => {
		if (props.to) {
			setLocation(props.to);
			window.scrollTo({top: 0, left: 0, behavior: "instant"})
		}
	};

	if (!props.noRipple) {
		return (
			<Ripples onClick={onClick}>
				<ButtonInner {...props} />
			</Ripples>
		);
	} else {
		return <ButtonInner onClick={onClick} {...props} />;
	}
});

export default Button;
