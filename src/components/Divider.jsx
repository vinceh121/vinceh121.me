import "./Divider.css";

function Divider({ large }) {
	return <div className={large ? "divider divider-large" : "divider"}></div>;
}

export default Divider;
