import {useLocation} from "wouter";
import {memo} from "react";

const Link = memo((props) => {
	const [_, setLocation] = useLocation();
	const myProps = {};

	if (props.to) {
		myProps.onclick = (e) => {
			e.preventDefault();
			setLocation(props.to);
			window.scrollTo({top: 0, left: 0, behavior: "instant"})
		};
		myProps.href = props.to;
	}

	if (props.href) {
		myProps.href = props.href;
	}

	return <a {...myProps}>{props.children}</a>;
});

export default Link;
