import './SimpleIcon.css';
import Tooltip from './Tooltip';

function SimpleIcon({ icon, brighten }) {
	const { title, svg, hex } = icon;

	return <Tooltip className={'simple-icon'} title={title}>
		<span
			dangerouslySetInnerHTML={{ __html: svg }}
			className={brighten ? 'simple-icon-bright' : ''}
			style={{ fill: '#' + hex }}
		></span>
	</Tooltip>;
}

export default SimpleIcon;
