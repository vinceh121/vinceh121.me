import "./Card.css";

function Card(props) {
	return <div className="card">{props.children}</div>;
}

function CardHeader(props) {
	return <div className="card-header">{props.children}</div>;
}

function CardAvatar(props) {
	if (!props.alt) throw new Error("Card avatar missing alt");
	return <img className="card-avatar" alt={props.alt} src={props.src} />;
}

function CardHeaderText(props) {
	return <div className="card-header-text">{props.children}</div>;
}

function CardTitle(props) {
	return <div className="card-title">{props.children}</div>;
}

function CardSubtitle(props) {
	return <div className="card-subtitle">{props.children}</div>;
}

export {
	Card,
	CardHeader,
	CardAvatar,
	CardHeaderText,
	CardTitle,
	CardSubtitle,
};
