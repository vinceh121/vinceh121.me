import "./CaptionedImage.css";
import { memo } from "react";

const CaptionedImage = memo((props) => {
	const { alt, src, description } = props;

	return <figure className="captioned-image">
		<img alt={alt} src={src} />
		<figcaption className="captioned-image-caption">
			<strong>{alt}</strong>
			<p>{description}</p>
		</figcaption>
	</figure>;
});

export default CaptionedImage;
