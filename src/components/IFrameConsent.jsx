import "./IFrameConsent.css";
import { useState } from "preact/hooks";

function IFrameConsent(props) {
	const [consent, setConsent] = useState(false);
	const { product, ...rest } = props;
	const { src } = props;

	const applyConsent = () => setConsent(true);
	const open = () => window.open(src, "_blank");

	if (consent) {
		return <iframe {...rest}></iframe>;
	} else {
		return <div className="iframe-consent" {...rest}>
			<h4>Consent of External Content</h4>
			<p>Displaying content from an external website, within another, can expose you to referral tracking.</p>
			<div>
				<button onClick={applyConsent} className="iframe-consent">Display content</button>
				<button onClick={open} className="iframe-open">Open content in new tab</button>
			</div>
			<p><a href="https://www.eff.org/deeplinks/2008/02/embedded-video-and-your-privacy" target="_blank">Learn more about embeds</a></p>
		</div>;
	}
}

export default IFrameConsent;
