import {
	Card,
	CardHeader,
	CardAvatar,
	CardHeaderText,
	CardTitle,
	CardSubtitle,
} from "./components/Card";
import Divider from "./components/Divider";
import FeatureList from "./components/FeatureList";
import { whereIWorked, thingsIDid, studies, thingsIDo, thingsIUse } from "./lists";
import { Pie } from "@nivo/pie";
import languagesJson from "./languages.json";
import chartTheme from "./chartTheme.json";
import { useEffect, useState } from "react";

function Home() {
	const [languages, setLanguages] = useState([]);

	useEffect(() => {
		// this effect is used so we can trigger graph animations
		setLanguages(languagesJson);
	}, [setLanguages]);

	return (
		<>
			<Card>
				<CardHeader>
					<CardAvatar
						src="/assets/profile.png"
						alt="Profile picture"
					/>
					<CardHeaderText>
						<CardTitle>Hello there</CardTitle>
						<CardSubtitle>Nerd extraordinaire</CardSubtitle>
					</CardHeaderText>
				</CardHeader>
				I'm a French guy that makes stuff in the hope that they'll be as
				useful to someone as they are for me.
			</Card>
			<Divider />
			<Card>
				<CardHeaderText>
					<CardTitle>My languages</CardTitle>
				</CardHeaderText>
				<div className="card-center">
					<Pie
						data={languages}
						margin={{ top: 80, right: 80, bottom: 80, left: 100 }}
						colors={{ datum: 'data.color' }}
						sortByValue={true}
						theme={chartTheme}
						arcLinkLabelsDiagonalLength={24}
						arcLinkLabelsStraightLength={16}
						activeOuterRadiusOffset={24}
						tooltip={() => {
						}}
						transitionMode="middleAngle"
						motionConfig="stiff"
						enableArcLabels={false}
						width={500}
						height={500} />
				</div>
			</Card>
			<Divider />
			<FeatureList features={thingsIDid} title="Things I did" />
			<Divider />
			<FeatureList features={whereIWorked} title="Where I worked" />
			<Divider />
			<FeatureList features={studies} title="Studies" />
			<Divider />
			<FeatureList features={thingsIDo} title="Things I do sometimes" />
			<Divider />
			<FeatureList features={thingsIUse} title="Stuff I use" />
		</>
	);
}

export default Home;
