import { Switch as Routes, Route } from "wouter";
import "./App.css";
import Button from "./components/Button";
import Home from "./Home";
import Contact from "./Contact";
import NotFound from "./NotFound";
import MediaTek from "./cned/MediaTek";
import MediaTekFormation from "./cned/MediatekFormation";
import PortfolioE4 from "./cned/PortfolioE4";
import MediaTekDocuments from "./cned/MediatekDocuments";
import france from "./components/France";

function App() {
	return (
		<>
			<header className="toolbar-header">
				<Button to="/" primary>
					<h2>vinceh121</h2>
				</Button>
				<span style={{ flex: "1 1 auto" }}></span>
				<Button to="/contact">Contact</Button>
			</header>
			<div className="content">
				<Routes>
					<Route path="/cned/mediatek" component={MediaTek} />
					<Route path="/cned/mediatekformation" component={MediaTekFormation} />
					<Route path="/cned/mediatekdocuments" component={MediaTekDocuments} />
					<Route path="/cned" component={PortfolioE4} />
					<Route path="/contact" component={Contact} />
					<Route exact path="/" component={Home} />
					<Route component={NotFound} />
				</Routes>
			</div>
			<footer>
				<div className="foot-elm">
					<Button href="https://www.gnu.org/licenses/gpl-3.0.en.html">
						License
					</Button>
					<Button href="https://gitlab.com/vinceh121/vinceh121.me">
						Source
					</Button>
				</div>
				<div className="foot-elm">
					<Button href="https://www.pexels.com/photo/architecture-buildings-city-fog-373965/">
						Background image
					</Button>
					<Button href="https://simpleicons.org/">Icons</Button>
				</div>
				<div className="foot-elm">
					<Button>Made with ❤️ in {france}</Button>
					<Button href="/.well-known/security.txt">Security</Button>
				</div>
			</footer>
		</>
	);
}

export default App;
